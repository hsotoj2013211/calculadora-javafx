package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Hermes
 */
public class FXMLDocumentController implements Initializable {
    @FXML 
    private AnchorPane panel;
    
    @FXML
    private TextField field;
    @FXML
    private Label label;
    
    @FXML 
    private Button boton;
    
    @FXML
    private void botonAccion(ActionEvent event) throws Exception {
       this.showText();
    }
    private void showText() throws Exception{
        try{
            double a=operar(field.getText());
            label.setText(String.valueOf((a)));
            
        }catch(Exception e){
            System.out.println("Error de sintaxis");
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    public static double operar(String operacion) throws Exception{
	return sumar(operacion);
    }
	
    private static double sumar(String operacion) throws Exception{
	String[] sumas = operacion.split("\\+");
	double resultado = 0;
	for(String suma : sumas){
            resultado+=restar(suma);
	}
	return resultado;
    }
    private static double restar(String operacion) throws Exception{
	String[] restas = operacion.split("\\-");
	double resultado = 0;
	for(String resta : restas){
            if(resultado==0) 
		resultado=multiplicar(resta);
            else
		resultado-=multiplicar(resta);
	}
	return resultado;
    }
    private static double multiplicar(String operacion) throws Exception{
        String[] multiplicaciones = operacion.split("\\*");
        double resultado = 1;
        for(String multiplicacion : multiplicaciones){
                resultado*=dividir(multiplicacion);
        }
        return resultado;
    }
    private static double dividir(String operacion) throws Exception{
        String[] divisiones = operacion.split("\\/");
        double resultado = 0;
        for(String division : divisiones){
                if(resultado==0)
                        resultado=Double.parseDouble(division);
                else
                        resultado/=Double.parseDouble(division);
        }
        return resultado;
    }
}
